FROM debian:latest

ENV OCL_DIR "/opt/intel/oclcpuexp"
ENV TBB_DIR "/opt/intel/tbb"
ENV OCL_VER "2020.10.6.0.4"
ENV TBB_VER "2020.2"

ENV SRC_URL "https://github.com/intel/compute-runtime/releases/download"
ENV NEO_DIR "/opt/intel/neo"
ENV NEO_VER "20.25.17111"

RUN apt-get update && apt upgrade -y && apt-get -y install python3-pip ocl-icd-opencl-dev clinfo wget git time \
    && mkdir -p ${OCL_DIR} ${TBB_DIR} /etc/OpenCL/vendors && cd ${OCL_DIR} \
    && wget https://github.com/intel/llvm/releases/download/2020-06/oclcpuexp-${OCL_VER}_rel.tar.gz \
    && tar zxvf oclcpuexp-*.tar.gz && rm oclcpuexp-*.tar.gz \
    && echo ${OCL_DIR}/x64/libintelocl.so > /etc/OpenCL/vendors/intel_expcpu.icd && cd ${TBB_DIR} \
    && wget https://github.com/oneapi-src/oneTBB/releases/download/v2020.2/tbb-${TBB_VER}-lin.tgz \
    && tar zxvf tbb*.tgz && rm tbb*.tgz \
    && cd ${OCL_DIR}/x64 && ln -s ${TBB_DIR}/tbb/lib/intel64/gcc4.8/libtbb.so . \
    && ln -s ${TBB_DIR}/tbb/lib/intel64/gcc4.8/libtbbmalloc.so . \
    && ln -s ${TBB_DIR}/tbb/lib/intel64/gcc4.8/libtbb.so.2 . \
    && ln -s ${TBB_DIR}/tbb/lib/intel64/gcc4.8/libtbbmalloc.so.2 . \
    && echo ${OCL_DIR}/x64 > /etc/ld.so.conf.d/libintelopenclexp.conf \
    && ldconfig -f /etc/ld.so.conf.d/libintelopenclexp.conf

RUN mkdir -p ${NEO_DIR} && cd ${NEO_DIR} \
    && wget ${SRC_URL}/${NEO_VER}/intel-gmmlib_20.1.1_amd64.deb \
    && wget ${SRC_URL}/${NEO_VER}/intel-igc-core_1.0.4155_amd64.deb \
    && wget ${SRC_URL}/${NEO_VER}/intel-igc-opencl_1.0.4155_amd64.deb \
    && wget ${SRC_URL}/${NEO_VER}/intel-opencl_${NEO_VER}_amd64.deb \
    && wget ${SRC_URL}/${NEO_VER}/ww25.sum && sha256sum --ignore-missing -c ww25.sum \
    && dpkg -i *.deb && rm *.deb ww25.sum

RUN mkdir /.cache && chmod -R 777 /.cache \
    && git clone https://github.com/rdemaria/simpletrack \
    && cd simpletrack && pip3 install pyopencl && pip3 install -e . 

COPY lhc/simpletrack/lhc-simpletrack.sh /simpletrack/examples/lhc/lhc-simpletrack.sh

ENTRYPOINT [ "/bin/bash", "-c", "cd /simpletrack/examples/lhc && ./lhc-simpletrack.sh \"$@\"", "--" ]

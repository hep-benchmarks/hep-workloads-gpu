# CMS GPU workloads

The sub-folders contain workloads provided by the CMS experiment that run on CPU+GPU system.
The reconstruction package is known as CMS Patratrack and is published in https://github.com/cms-patatrack

We use it to build a CPU+GPU benchmark workload, following the same approaches developed for the HEP-workloads targetting CPUs [HEP workloads](https://gitlab.cern.ch/hep-benchmarks/hep-workloads)
The purpose of this hep-workloads-gpu gitlab project is to build standalone container including software, data and orchestrator procedures needed to run the CMS workload as a benchmark.
For this purpose a limited set of events is used to run the reconstruction workload and measure the performance in terms of event throughput.

The procedure to build the standalone container is documented in the gitlab CI [yml file](https://gitlab.cern.ch/hep-benchmarks/hep-workloads-gpu/-/blob/qa/cms/cms-patatrack-ci.yml)

In order to run the standalone container follow these steps and look for results in the defined RESULTS_DIR

As image use the most recent tag version or the qa one, as reported in the [registry](https://gitlab.cern.ch/hep-benchmarks/hep-workloads-gpu/container_registry/eyJuYW1lIjoiaGVwLWJlbmNobWFya3MvaGVwLXdvcmtsb2Fkcy1ncHUvY21zL2Ntcy1wYXRhdHJhY2stbnZpZGlhLWJtayIsInRhZ3NfcGF0aCI6Ii9oZXAtYmVuY2htYXJrcy9oZXAtd29ya2xvYWRzLWdwdS9yZWdpc3RyeS9yZXBvc2l0b3J5LzcwNTcvdGFncz9mb3JtYXQ9anNvbiIsImlkIjo3MDU3fQ==)

```
export RESULTS_DIR=/any_path_you_like
export IMAGE_NAME=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-gpu/cms/cms-patatrack-nvidia-bmk:qa
docker pull ${IMAGE_NAME}
docker run --rm --gpus '"device=0"' -v ${RESULTS_DIR}:/results ${IMAGE_NAME}
```
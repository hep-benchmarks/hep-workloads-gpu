#!/bin/bash

# script used in gitlab CI
# for job snapshot_cvmfs
# in file cms/cms-patatrack-ci.yml

function _before_script() {
    docker pull ${CVMFS_IMAGE}
    docker run --name cvmfs_${CI_JOB_ID} -d --privileged -v ${CVMFS_EXPORT_DIR}:${CVMFS_EXPORT_DIR} -v ${CIENV_CVMFSVOLUME}:/cvmfs:shared ${CVMFS_IMAGE} -r ${CIENV_CVMFSREPO} -t /tmp/traces
}

function _script() {
    sleep 1m # to give time to cvmfs to start
    echo "CVMFS_EXPORT_DIR is $CVMFS_EXPORT_DIR"
    # check cvmfs is running
    docker exec cvmfs_${CI_JOB_ID} cvmfs_config probe
    # Here comes the dry run of the CMS Patatrack container. Arguments are for the time being defaults/hardcoded FIXME
    docker pull ${INTERIM_IMAGE}
    docker run --name patatrack_container --gpus '"device=0"' -v ${CIENV_CVMFSVOLUME}:/cvmfs ${INTERIM_IMAGE} -e 100 -t 8 -c 1
    # run shrinkwrapper
    docker exec cvmfs_${CI_JOB_ID} /root/shrinkwrap.sh -t /tmp/traces -e ${CVMFS_EXPORT_DIR}
    # get the generated traces from the container
    docker cp cvmfs_${CI_JOB_ID}:/tmp/traces ${CI_PROJECT_DIR}/traces
    # TODO: check if fixed in CI via traces/cms.cern.ch_spec_custom.txt
    # FIXME this is a dirty patch needed to make scipy running. cvmfs shrinkwrapper alone does not copy all files of that dir. To be investigated why
    ls -lR ${CVMFS_EXPORT_DIR}/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/py2-scipy/1.2.3-bcolbf/lib/python2.7 >${CI_PROJECT_DIR}/cvmfs_export_py2-scipy_content
    rm -fr ${CVMFS_EXPORT_DIR}/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/py2-scipy/1.2.3-bcolbf/lib/python2.7/site-packages
    docker cp patatrack_container:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/py2-scipy/1.2.3-bcolbf/lib/python2.7/site-packages ${CVMFS_EXPORT_DIR}/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/py2-scipy/1.2.3-bcolbf/lib/python2.7
    # TODO: include sha5checksum of exported CVMFS?
    # tar -cf /tmp/cvmfs_checksum.tar ${CVMFS_EXPORT_DIR}/cvmfs && md5sum /tmp/cvmfs_checksum.tar | cut -f1 -d" " > /tmp/cvmfs_checksum.txt && rm /tmp/cvmfs_checksum.tar
    # mv /tmp/cvmfs_checksum.txt ${CVMFS_EXPORT_DIR}/cms/patatrack/cvmfs
    # remove duplicated data & empty dirs
    # find ${CVMFS_EXPORT_DIR} -type d -empty -delete
    rm -rf ${CVMFS_EXPORT_DIR}/cvmfs/.data
    ls -R ${CVMFS_EXPORT_DIR} >${CI_PROJECT_DIR}/cvmfs_export_dir_content.txt
}

function _after_script() {
    #sigkill and remove containers, images remain.
    docker rm -f cvmfs_${CI_JOB_ID}
    docker rm -f patatrack_container
}

# TODO: clean up $CIENV_CVMFSVOLUME, clean up docker image cache
export CIENV_CVMFSVOLUME=/scratch/cvmfs_hep/CI-JOB-${CI_JOB_ID}
export CVMFS_EXPORT_DIR=${CI_PROJECT_DIR}/cms/patatrack
export CIENV_CVMFSREPO=cms.cern.ch
export CVMFS_IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-builder/cvmfs-image:qa
export INTERIM_IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-gpu/cms/cms-patatrack-nvidia-interim:${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
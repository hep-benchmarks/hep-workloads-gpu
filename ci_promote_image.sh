#!/bin/bash

 echo "current CI_COMMIT_BRANCH is ${CI_COMMIT_BRANCH}"
 echo "current CI_COMMIT_REF_NAME is $CI_COMMIT_REF_NAME"
 echo "current CI_COMMIT_SHA is ${CI_COMMIT_SHA:0:8}"
 echo "current CI_COMMIT_TAG is ${CI_COMMIT_TAG}"
 if [[ -z $IMAGE_NAMES ]]; then echo "ERROR variable IMAGE_NAMES is not defined "; exit 1; fi
 if [[ -z $IMAGE_TAG ]]; then echo "ERROR variable IMAGE_TAG is not defined "; exit 1; fi
 if [[ -z $CI_COMMIT_TAG ]]; then echo "ERROR variable CI_COMMIT_TAG is not defined "; exit 1; fi 
 NEW_IMAGE_TAG=`echo ${CI_COMMIT_TAG} | sed -e 's@.*-\(v[0-9\.]*\)@\1@'`
 echo "new IMAGE TAG is ${NEW_IMAGE_TAG}"
 docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN gitlab-registry.cern.ch
 for IMAGE_NAME in `echo $IMAGE_NAMES | tr '|' '\n'`;
 do
    echo "IMAGE to tag is ${IMAGE_NAME}:${IMAGE_TAG}"
    docker pull ${IMAGE_NAME}:${IMAGE_TAG}
    docker tag ${IMAGE_NAME}:${IMAGE_TAG} ${IMAGE_NAME}:${NEW_IMAGE_TAG}
    docker tag ${IMAGE_NAME}:${IMAGE_TAG} ${IMAGE_NAME}:latest
    docker push ${IMAGE_NAME}:${NEW_IMAGE_TAG}
    docker push ${IMAGE_NAME}:latest
    docker rmi ${IMAGE_NAME}:${IMAGE_TAG} ${IMAGE_NAME}:${NEW_IMAGE_TAG} ${IMAGE_NAME}:latest
done